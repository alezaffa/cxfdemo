package it.azware.cxf.CxfDemo;

import javax.jws.WebService;

@WebService(endpointInterface = "it.azware.cxf.CxfDemo.HelloWorld")
public class HelloWorldImpl implements HelloWorld {
	public String sayHi(String text) {
		System.out.println("sayHi called");
		return "Hello " + text;
	}
}