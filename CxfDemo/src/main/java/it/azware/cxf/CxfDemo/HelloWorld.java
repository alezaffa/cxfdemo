package it.azware.cxf.CxfDemo;

import javax.jws.WebService;

@WebService
public interface HelloWorld {
	String sayHi(String text);
}
